package com.company;

import org.kopitubruk.util.json.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by traed on 10/26/2016.
 */
public class Communicator {

    DatagramSocket aSocket;
    InetAddress aHost;
    DatagramPacket request;
    int numConnections;
    HashMap<String,HashMap<String,Integer>> connections;
    public Communicator(File c){
        try {
            aSocket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        try {
            connections = (HashMap<String, HashMap<String, Integer>>) JSONParser.parseJSON(new FileReader(c));
        } catch (IOException e) {
            e.printStackTrace();
        }
        numConnections=connections.keySet().size();
    }

    public void sendMessage(byte[] m, String BankId){
            for (String host : connections.get(BankId).keySet()){
                try {
                    aHost = InetAddress.getByName(host);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                String i = String.valueOf(connections.get(BankId).get(host));


                request = new DatagramPacket(m,  m.length, aHost, Integer.valueOf(i));
                try {
                    aSocket.send(request);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }



    }

    public void Snapshot(long id, int requester){
        for (String bank:connections.keySet()) {
            for (String host : connections.get(bank).keySet()) {
                byte[] m = ("{\"Snap\":{\"buffer\":{\"snapId\":" + id + ",\"requester\":"+requester+"}}}").getBytes();
                try {
                    aHost = InetAddress.getByName(host);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                String i = String.valueOf(connections.get(bank).get(host));

                request = new DatagramPacket(m,  m.length, aHost, Integer.valueOf(i));
                try {
                    aSocket.send(request);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}



