package com.company;

import org.kopitubruk.util.json.JSONParser;
import org.kopitubruk.util.json.JSONUtil;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by traed on 10/26/2016.
 */
public class Bank implements WindowListener, Runnable {

    int id;
    HashMap<String,HashMap<String,Double>> Balances;
    HashMap<String,String> Accounts;
    Communicator c1 ;
    MessagingQue q1;
    String CurrentUser;
    ArrayList<Long> SnapShotIds;
    HashMap<String,HashMap<String,Double>> temp;
    private  String requester;
    private JFrame frame;
    private JTextField textField = new JTextField();
    private JTextField textField_1 = new JTextField();
    private JTextArea textField_2;
    private JTextField textField_3= new JTextField();
    private JTextField textField_4 = new JTextField();
    boolean windowClosed = false;

    public Bank(int id) {
        this.id = id;
        initialize();
        windowClosed = true;
        SnapShotIds = new ArrayList<>();
        try {
            Balances = (HashMap<String, HashMap<String, Double>>) JSONParser.parseJSON(new FileReader("Balances"+id+".txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        q1 = new MessagingQue(id);
        try {
            Accounts = (HashMap<String, String>) JSONParser.parseJSON(new FileReader("Accounts"+id+".txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        c1 = new Communicator(new File("Connections"+id+".txt"));

        textField_2.setText(Balances.toString());

    }

    public boolean Login(String u,String password){

            if(Accounts.containsKey(u) && Accounts.get(u).equals(password)){
                CurrentUser = u;
                textField_1.setText(String.valueOf(Balances.get("Bank"+id).get(CurrentUser)));
                return  true;
            }
            else return  false;
    }

    public void Transfer(double Amount,String To,int BankId){
        HashMap<String,HashMap<String,Double>> t = new HashMap<>();
        HashMap<String,Double> c = new HashMap<>();
        c.put(CurrentUser,-Amount);
        t.put("Bank"+id,c);
        c= new HashMap<>();
        c.put(To,Amount);
        t.put("Bank"+BankId,c);

        c1.sendMessage(("{transfer:"+JSONUtil.toJSON(t)+"}").getBytes(),"Bank"+BankId);
        UpdateAccounts(t);
    }

    public void UpdateAccounts(HashMap<String,HashMap<String,Double>> t){
        for (String i : t.keySet()) {
            for (String k:t.get(i).keySet()) {
                String b1=  String.valueOf(Balances.get(i).get(k));
                String b2 =String.valueOf(t.get(i).get(k));
                Balances.get(i).put(k,Double.valueOf(b1)+Double.valueOf(b2));
            }

        }
        textField_1.setText(String.valueOf(Balances.get("Bank"+id).get(CurrentUser)));
        textField_2.setText(Balances.toString());
    }

    public  void SnapShot(){ requester = "";c1.Snapshot(System.currentTimeMillis() / 1000L,id); temp = new HashMap<>();temp.put("Bank"+id,Balances.get("Bank"+id));}
    @Override
    public void run() {
        JTextField username = new JTextField();
        JTextField password = new JPasswordField();
        boolean logSucess = false;
        Object[] message = {
                "Username:", username,
                "Password:", password
        };
        while (!logSucess) {
            int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
            if (option == JOptionPane.OK_OPTION) {
                if (Login(username.getText(), password.getText())) {
                    logSucess = true;
                }
                if (!logSucess)
                    JOptionPane.showConfirmDialog(null, "Enter Good Credentials", "Login", JOptionPane.ERROR_MESSAGE);
            }

            frame.setVisible(true);
            frame.addWindowListener(this);
            while (1 != 0) {


                byte[] b = q1.getMessage();
                if (b != null) {
                    String m = new String(b);
                    HashMap<String, HashMap<String, HashMap<String, Double>>> Data = (HashMap) JSONParser.parseJSON(m);
                    String event = (String) Data.keySet().toArray()[0];
                    switch (event) {
                        case "transfer":
                            UpdateAccounts(Data.get(event));
                            break;
                        case "Snap":

                            Long l = new Long(String.valueOf(Data.get("Snap").get("buffer").get("snapId")));
                            if (temp == null && !SnapShotIds.contains(l)) {
                                c1.Snapshot(l, id);
                                temp = new HashMap<>();
                                temp.put("Bank" + id, Balances.get("Bank" + id));
                                SnapShotIds.add(l);
                                requester = "Bank" + String.valueOf(Data.get("Snap").get("buffer").get("requester"));
                            } else {
                                if (!requester.equals("")) {
                                    c1.sendMessage(("{shot:" + JSONUtil.toJSON(temp) + "}").getBytes(), requester);
                                    temp = null;
                                }
                            }
                            break;
                        case "shot":
                            if (temp == null)
                                temp = new HashMap<>();
                            temp.putAll(Data.get("shot"));

                            if (requester.equals("") && temp.keySet().size() == Balances.keySet().size()) {

                                Balances = temp;
                                temp = null;
                                textField_2.setText(Balances.toString());
                                SnapShot();
                                break;
                            }
                            break;
                    }
                }


                if (temp != null && temp.keySet().size() == c1.numConnections && !requester.equals("")) {
                    c1.sendMessage(("{shot:" + JSONUtil.toJSON(temp) + "}").getBytes(), requester);
                    temp = null;
                }


            }
        }
    }
    private void initialize() {
        frame = new JFrame();
        textField_4 = new JTextField();
        frame.setBounds(100, 100, 350, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setResizable(false);
        JButton btnTransfer = new JButton("Transfer");
        btnTransfer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(textField.getText());
                System.out.println(textField_4.getText());
                System.out.println(textField_3.getText());
                Transfer(Double.parseDouble(textField.getText()),textField_4.getText(),Integer.parseInt(textField_3.getText()));
            }
        });
        btnTransfer.setBounds(41, 301, 89, 23);
        frame.getContentPane().add(btnTransfer);

        JButton btnSnapshot = new JButton("Snapshot");
        btnSnapshot.setBounds(187, 301, 89, 23);
        btnSnapshot.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SnapShot();

            }
        });
        frame.getContentPane().add(btnSnapshot);



        JLabel lblAmountToTransfer = new JLabel("Amount to Transfer");
        lblAmountToTransfer.setBounds(29, 30, 132, 14);
        frame.getContentPane().add(lblAmountToTransfer);

        textField = new JTextField();
        textField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });



        textField.setBounds(155, 27, 86, 20);
        frame.getContentPane().add(textField);
        textField.setColumns(10);

        textField_1 = new JTextField();
        textField_1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
            }
        });
        textField_1.setBounds(155, 69, 86, 20);
        frame.getContentPane().add(textField_1);
        textField_1.setColumns(10);

        JLabel lblCurrentBalance = new JLabel("Current Balance");
        lblCurrentBalance.setBounds(29, 72, 116, 14);
        frame.getContentPane().add(lblCurrentBalance);

        JLabel lblBalances = new JLabel("Balances:");
        lblBalances.setBounds(41, 108, 89, 14);
        frame.getContentPane().add(lblBalances);

        textField_2 = new JTextArea();
        textField_2.setLineWrap(true);
        textField_2.setBounds(41, 133, 249, 137);
        frame.getContentPane().add(textField_2);
        textField_2.setColumns(10);


        JLabel lblBankId = new JLabel("Bank to Transfer");
        lblBankId.setBounds(29, 10, 132, 14);
        frame.getContentPane().add(lblBankId);

        textField_3 = new JTextField();
        textField_3.setBounds(155, 10, 85, 14);
        frame.getContentPane().add(textField_3);
        textField_3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });

        JLabel lblUserID = new JLabel("UserId:");
        lblUserID.setBounds(40, 50, 85, 14);
        frame.getContentPane().add(lblUserID);

        textField_4.setBounds(100, 50, 85, 14);
        frame.getContentPane().add(textField_4);
        textField_4.setColumns(10);
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
