package com.company;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by traed on 10/26/2016.
 */
public class MessagingQue {
    BlockingQueue<byte[]> queue = new LinkedBlockingQueue<>();
    Listener l;
    Thread worker;
    public MessagingQue(int Interface){
        l = new Listener(Interface,queue);
        worker = new Thread(l);
        worker.start();

    }
    public void kill(){
        l.die= true;
    }
    public byte[] getMessage(){


            return queue.poll();


    }
    public class Listener implements Runnable{

    DatagramSocket aSocket;
    DatagramPacket request;
    BlockingQueue<byte[]> queue;
    int messageLength = 10000;
    byte[] newMessage = new byte[messageLength];
    Boolean die = false;

    public Listener(int Interface, BlockingQueue<byte[]> que){
        queue = que;
        try {
            newMessage = new byte[messageLength];
            request = new DatagramPacket(newMessage, newMessage.length);
            aSocket = new DatagramSocket(Interface);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {
        while(!die){
            try {
                aSocket.receive(request);
                queue.put(request.getData());
                newMessage = new byte[messageLength];
                request = new DatagramPacket(newMessage, newMessage.length);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}
}

